package com.example.stompdemo.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * date 2021/3/11
 */
@Controller
public class PageController {

    @RequestMapping("userList")
    public String getUserList(Model model){
        List<Map<String, String>> list = SocketManager.getArrayList();
        model.addAttribute("list", list);
        return "userList";
    }
}
