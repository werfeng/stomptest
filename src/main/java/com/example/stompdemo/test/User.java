package com.example.stompdemo.test;

import javax.security.auth.Subject;
import java.security.Principal;

/**
 * date 2021/3/5
 */
public class User implements Principal {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }
}
