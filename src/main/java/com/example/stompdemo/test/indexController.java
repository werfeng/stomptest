package com.example.stompdemo.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * date 2021/3/5
 */
@Controller
public class indexController {
    @RequestMapping("/")
    public String index(){
        return "websocket";
    }
}
