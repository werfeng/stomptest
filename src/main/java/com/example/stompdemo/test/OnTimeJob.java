//package com.example.stompdemo;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
///**
// * date 2021/3/5
// */
//@Component
//public class OnTimeJob {
//
//    @Autowired
//    private SimpMessagingTemplate simpMessagingTemplate;
//
//    @Scheduled(cron = "0/2 * * * * ?")
//    public void sendMessage(){
//        //发送广播
//        simpMessagingTemplate.convertAndSend("/topic/greetings1","topic推送消息");
////        一对一(发送给用户)
////        simpMessagingTemplate.convertAndSendToUser("username","/message","推送消息");
//
//    }
//}
